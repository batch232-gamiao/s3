package com.zuitt.example;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        HashMap<String, Integer> games = new HashMap<>(){
            {
                put("Mario Odyssey", 50);
                put("Super Smash Bros. Ultimate", 20);
                put("Luigi's Mansion 3", 15);
                put("Pokemon Sword", 30);
                put("Pokemon Shield", 100);
            }
        };
        ArrayList<String> topGames = new ArrayList<>();

        // stocks left
        games.forEach((key,value) ->{
            System.out.println(key + " has " + value + " stocks left.");
        });
        // top games
        System.out.println("");
        games.forEach((key,value) ->{
            if(value <= 30){
                topGames.add(key);
                System.out.println(key + " has been added to top games list!");
            }
        });

        System.out.println("Our shop's top games:");
        System.out.println(topGames);
        System.out.println("");
        // Stretch Goal ------------------------------
        boolean addItem = true;

        Scanner scanner = new Scanner(System.in);

        while(addItem){
            System.out.println("Would you like to add an item? Yes or No.");
                HashMap<String, Integer> product = new HashMap<>();
                String itemName, input;
                int itemStock;

                input = scanner.nextLine();
                if(input.equals("Yes") || input.equals("yes")){
                    System.out.println("Add the item name:");
                    itemName = scanner.nextLine();
                    System.out.println("Add the item stock:");
                    itemStock = scanner.nextInt();

                    product.put(itemName,itemStock);
                    System.out.println("You have successfully added a product! - " + product);
                    addItem = false;
                }
                else if(input.equals("No") || input.equals("no")){
                    addItem = false;
                    System.out.println("Thank you");
                }
                else {
                    System.out.println("Invalid input. Try again");
                }
        }
    }
}
